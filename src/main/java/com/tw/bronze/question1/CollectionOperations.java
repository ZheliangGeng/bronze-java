package com.tw.bronze.question1;

import java.util.ArrayList;
import java.util.Collections;

public class CollectionOperations {
    /**
     * This method will remove all the duplicated `String` values from a
     * collection.
     *
     * @param values The collection which may contain duplicated values
     * @return A new array removing the duplicated values.
     */
    public static String[] distinct(Iterable<String> values) {
        // TODO:
        //  Please implement the method. If you have no idea what Iterable<T>
        //  is, please refer to the unit test, or you can have a search on
        //  the internet, or you can find it in our material.
        // <-start-
        if (values == null) {
            throw new IllegalArgumentException();
        }
        ArrayList<String> arrayTemp= new ArrayList<String>();
        for(String value: values){
            do{
                arrayTemp.add(value);
            }while(arrayTemp.contains(value) == false)
        }
        String[] arrayWithoutDup = arrayTemp.toArray(new String[0]);
        Collections.sort(arrayWithoutDup);
        return arrayWithoutDup;
        // --end->
    }
}
